/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Messages from './src/screens/Messages';
import Chat from './src/screens/Chat';
import RegisterScreen from './src/screens/RegisterScreen';
import Router from './src/navigation/Router';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Router);
