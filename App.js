import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

const App = () => {
  return (
    <View>
      <Text style={{color: '#000'}}>App</Text>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({});
