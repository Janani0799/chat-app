import {Text, View, Image, Dimensions, TouchableOpacity} from 'react-native';
import React from 'react';
import {adjust, SIZES, FONTS, COLORS} from '../../constants';
import {ScaledSheet} from 'react-native-size-matters';

export const CustomCard = ({title, image, text, onClick}) => {
  return (
    <TouchableOpacity style={styles.cardView} onPress={onClick}>
      <View style={{flex: 0.2, justifyContent: 'center', alignItems: 'center'}}>
        <Image
          style={styles.image}
          resizeMode={'cover'}
          source={{uri: image}}
        />
      </View>
      <View
        style={{
          flex: 0.88,
          justifyContent: 'center',
          borderBottomColor: '#9B9B9B',
          borderBottomWidth: 1,
          paddingTop: 10,
          paddingBottom: 18,
        }}>
        <View style={{flexDirection: 'row'}}>
          <Text numberOfLines={1} style={styles.titletxt}>
            {title}
          </Text>
          <View style={{flex: 0.2, justifyContent: 'center'}}></View>
        </View>
        <Text numberOfLines={1} style={styles.txt}>
          {text}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = ScaledSheet.create({
  cardView: {
    flexDirection: 'row',
    backgroundColor: COLORS.whiteColor,
    //width: "330@s",
    width: SIZES.width,
    height: '72@vs',
    //paddingVertical: "10@vs",
    borderRadius: '4@msr',
    //marginTop: "5@vs",
    alignSelf: 'center',
    // justifyContent:'center'
    alignItems: 'center',
    paddingHorizontal: '15@s',
  },
  image: {
    //alignSelf: "flex-start",
    width: '50@s',
    height: '50@vs',
    //marginHorizontal: "10@s",
    //marginVertical: "3@vs",
    borderRadius: '40@msr',
    alignSelf: 'flex-start',
  },
  titletxt: {
    //...FONTS.h3,
    fontSize: adjust(16),
    fontWeight: '600',
    flex: 0.75,
    color: COLORS.blackColor,
    lineHeight: '25@vs',
  },
  txt: {
    //...FONTS.h5,
    fontSize: adjust(14),
    fontWeight: '500',
    color: '#B3B3B3',
  },
});
