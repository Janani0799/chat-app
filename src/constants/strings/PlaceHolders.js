//Login
const loginTitle = 'Login';
const welcome = 'Welcome to Smart Chat!';

//Register
const RegisterTitle = 'Enter the username to continue :';
const GoToText = 'Go to the Chat Screen';


export default {
  loginTitle,
  welcome,
  RegisterTitle,
  GoToText,
};
