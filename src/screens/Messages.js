import React from 'react';
import {
  Text,
  View,
  SafeAreaView,
  FlatList,
  StatusBar,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import database from '@react-native-firebase/database';
import {COLORS, FONTS, adjust} from '../constants';
import {CustomCard} from '../components/card/CustomCard';


const Messages = ({navigation, route}) => {
  const {passnum,passname} = route.params;

 
  const [itemsArray, setItemsArray] = React.useState([]);
  React.useEffect(() => {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      const users = Object.values(data);
      setItemsArray(users);
    });
   
  }, []);


  let itemsRef = database().ref(`/users`);

  let passno = passnum;

  let filteredData = itemsArray.filter(items => items.number !== passnum);
  console.log(filteredData);

  const chatHistoryCreate = item => {
    let numbers = passno.concat(item.number);
    console.log('num',numbers);
    if (item.hasOwnProperty('chat_id')) {
      if (item.chat_id.hasOwnProperty(passno)) {
        console.log('item.chat_id', item.number);
        const dd = database()
          .ref(`/users/${item.number}/chat_id/${passno}`)
          .once('value')
          .then(snapshot => {
            console.log('User data: ', snapshot.val());
            let getData = snapshot.val();
            navigation.navigate('Chat', {chatID: getData.user_id,passNames:item.name});
          });
      } else {
        database()
          .ref(`/users/${passno}/chat_id/${item.number}/`)
          .set({
            user_id: numbers,
          })
          .then(() => {
            database()
              .ref(`/users/${item.number}/chat_id/${passno}/`)
              .set({
                user_id: numbers,
              })
              .then(() => {
                navigation.navigate('Chat', {chatID: numbers,passNames:item.name});
              });
          });
      }
    } else {
      database()
        .ref(`/users/${passno}/chat_id/${item.number}/`)
        .set({
          user_id: numbers,
        })
        .then(() => {
          database()
            .ref(`/users/${item.number}/chat_id/${passno}/`)
            .set({
              user_id: numbers,
            })
            .then(() => {
              navigation.navigate('Chat', {chatID: numbers,passNames:item.name});
              console.log('found',item.name)
            });
        });
    }
  };
  

  const ListItem = ({item}) => {
    return (
      <CustomCard
        image={item.image}
        title={item.name}
        text={item.number}
        onClick={ 
          () => chatHistoryCreate(item)
        }
      />
    );
  };

  const FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '75%',
          backgroundColor: '#9B9B9B',
          marginHorizontal: 15,
          marginLeft: 75,
        }}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        backgroundColor={COLORS.whiteColor}
        barStyle={'dark-content'}
      />
      <View style={styles.header}>
        <Text style={styles.headerTxt}>Messages</Text>
        <Text style={styles.nameTxt}>{passname}</Text>
      </View>
      <View>
        <FlatList
          data={filteredData}
          renderItem={item => ListItem(item)}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.flatListBottom}
          //ItemSeparatorComponent={FlatListItemSeparator}
        />
      </View>
    </SafeAreaView>
  );
};

export default Messages;

const styles = ScaledSheet.create({
  container: {flex: 1, backgroundColor: COLORS.whiteColor},
  header: {
    height: '80@vs',
    backgroundColor: COLORS.primaryColor,
    justifyContent: 'center',
    paddingHorizontal: '20@s',
  },
  headerTxt: {
    color: COLORS.whiteColor,
    ...FONTS.h2,
    fontSize: adjust(22),
  },
  nameTxt: {
    color: COLORS.whiteColor,
    ...FONTS.h2,
    fontSize: adjust(15),
  },
  flatListBottom: {paddingBottom: '100@vs'},
});
