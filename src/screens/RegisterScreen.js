import React from 'react';
import {
  Text,
  View,
  SafeAreaView,
  TextInput,
  Image,
  StatusBar,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import database from '@react-native-firebase/database';
import {v4 as uuidv4} from 'uuid';
import 'react-native-get-random-values';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {COLORS, FONTS, SIZES, Images, PlaceHolders} from '../constants';
import {allowNumOnly} from '../utils';

const RegisterScreen = ({navigation}) => {
  const [name, setName] = React.useState('');
  const [number, setNumber] = React.useState(0);

  const moment = require('moment');
  const time = moment();

  const handleSubmit = () => {
    if (name === '') {
      Alert.alert('Chat App', 'Please enter the name');
    } else if (number === '') {
      Alert.alert('Chat App', 'Please enter the number');
    } else if (number.length !== 10) {
      Alert.alert('Chat App', 'Mobile number must be 10 digit');
    } else {
      console.log('show');
      const reference = database()
        .ref()
        .child('users')
        .orderByChild('num')
        .equalTo(number)
        .once('value')
        .then(snapshot => {
          if (snapshot.exists()) {
            let userData = snapshot.val();
            console.log(userData);
            let passData = number;
            let passuser = name;
            navigation.navigate('Messages', {
              passnum: passData,
              passname: passuser,
            });
          } else {
            console.log('not found');
            database()
              .ref(`/users/${number}`)
              .set({
                name: name,
                image:
                  'https://previews.123rf.com/images/koblizeek/koblizeek2001/koblizeek200100050/138262629-man-icon-profile-member-user-perconal-symbol-vector-on-white-isolated-background-.jpg',
                unix: time.unix(),
                utc: time.format('YYYY-MM-DD HH:mm:ss'),
                uuid: uuidv4(),
                number: number,
              });
            let passData = number;
            let passuser = name;
            navigation.navigate('Messages', {
              passnum: passData,
              passname: passuser,
            });
          }
          setName('');
          setNumber(0);
        });
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        backgroundColor={COLORS.whiteColor}
        barStyle={'dark-content'}
      />
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps={'handled'}
        scrollEnabled={true}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{flexGrow: 1}}>
        <View>
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <Image source={Images.welcome} style={styles.image} />
          </View>
          <Text style={styles.welcomeTxt}>{PlaceHolders.welcome}</Text>
          <Text style={styles.enterTxt}>{PlaceHolders.RegisterTitle}</Text>
          <TextInput
            style={styles.textInput}
            value={name}
            onChangeText={text => setName(text)}
            selectionColor={COLORS.primaryColor}
          />
          <Text style={styles.enterTxt}>{'Enter the phone number :'}</Text>
          <TextInput
            style={styles.textInput}
            keyboardType={'numeric'}
            value={number}
            onChangeText={text => setNumber(allowNumOnly(text))}
            selectionColor={COLORS.primaryColor}
          />

          <TouchableOpacity
            style={styles.button}
            onPress={() => handleSubmit()}>
            <Text style={styles.buttonTxt}>{'Submit'}</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default RegisterScreen;

const styles = ScaledSheet.create({
  container: {flex: 1, backgroundColor: COLORS.whiteColor},
  image: {
    height: '350@vs',
    width: SIZES.width,
  },
  textInput: {
    backgroundColor: COLORS.whiteColor,
    paddingVertical: '6@vs',
    paddingHorizontal: '15@s',
    borderRadius: '25@msr',
    marginVertical: '10@vs',
    borderColor: COLORS.lightGrey,
    borderWidth: '1@s',
    width: SIZES.width - 40,
    alignSelf: 'center',
  },
  enterTxt: {
    ...FONTS.h5,
    color: COLORS.darkGrey,
    paddingHorizontal: '22@s',
    marginTop: '10@vs',
  },
  welcomeTxt: {
    ...FONTS.h2,
    fontWeight: '700',
    color: COLORS.primaryColor,
    alignSelf: 'center',
  },
  button: {
    backgroundColor: COLORS.primaryColor,
    paddingVertical: '9@vs',
    paddingHorizontal: '15@s',
    borderRadius: '20@msr',
    marginVertical: '25@vs',
    alignSelf: 'center',
    width: SIZES.width * 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonTxt: {
    color: COLORS.whiteColor,
    ...FONTS.h4,
  },
  goToText: {
    ...FONTS.h5,
    color: COLORS.primaryColor,
    textDecorationLine: 'underline',
    textAlign: 'center',
  },
});
