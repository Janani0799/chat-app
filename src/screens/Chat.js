import React, {useState} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  FlatList,
  Image,
  StatusBar,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {COLORS, FONTS, SIZES, Vector} from '../constants';
import database from '@react-native-firebase/database';

const Chat = ({navigation, route}) => {
  const [inputText, setInputText] = useState('');
  const [data, setData] = useState([]);

  const {chatID, passNames} = route.params;
  console.log('num1', chatID);

  React.useEffect(() => {
    database()
      .ref(`/chat_history/${chatID}`)
      .on('value', snapshot => {
        let receiverData = snapshot.val();
        if (receiverData !== null) {
          console.log('dd', data);
          const items = Object.values(receiverData);
          setData(items);
        } else {
          console.log('dd', data);
        }
      });
  }, []);

  const Bindup = () => {
    if (inputText !== '') {
      database().ref(`/chat_history/${chatID}`).push({
        text: inputText,
      });
      setTimeout (() => {
        setInputText ('');
      }, 200);
    } else {
      alert('Type Something');
    }
   
  };

  const ChatList = ({item}) => {
    return (
      <View>
        <View style={item.isSender ? styles.receiverList : styles.senderList}>
          <Text style={item.isSender ? styles.receiverTxt : styles.senderTxt}>
            {item.text}
          </Text>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        backgroundColor={COLORS.whiteColor}
        barStyle={'dark-content'}
      />
      <View style={styles.header}>
        <TouchableOpacity
          style={{flex: 0.13}}
          onPress={() => navigation.goBack()}>
          {Vector.BackArrow}
        </TouchableOpacity>
        <View style={{flex: 0.22}}>
          <Image
            source={{
              uri: 'https://previews.123rf.com/images/koblizeek/koblizeek2001/koblizeek200100050/138262629-man-icon-profile-member-user-perconal-symbol-vector-on-white-isolated-background-.jpg',
            }}
            style={styles.image}
          />
        </View>
        <View style={{flex: 0.9}}>
          <Text style={styles.name}>{passNames}</Text>
          {/* <Text style={styles.userName}>{'Laura07@'}</Text> */}
        </View>
      </View>

      <FlatList
        inverted={true}
        data={data}
        renderItem={item => ChatList(item)}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.flatListBottom}
      />

      <View style={styles.bottomView}>
        <TouchableOpacity style={{flex: 0.1, marginLeft: 12}}>
          {Vector.Plus}
        </TouchableOpacity>
        <TextInput
          style={styles.textInput}
          multiline={true}
          value={inputText}
          onChangeText={text => setInputText(text)}
          selectionColor={COLORS.primaryColor}
        />
        <View style={{flex: 0.15, marginLeft: 12}}>
          <TouchableOpacity activeOpacity={0.6} onPress={Bindup}>
            {Vector.Send}
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Chat;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.whiteColor,
    alignItems: 'center',
  },
  header: {
    height: '65@vs',
    backgroundColor: COLORS.primaryColor,
    justifyContent: 'center',
    paddingHorizontal: '12@s',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: '42@s',
    height: '42@vs',
    borderRadius: '40@msr',
    alignSelf: 'flex-start',
  },
  name: {
    color: COLORS.whiteColor,
    ...FONTS.h3,
  },
  userName: {
    color: COLORS.whiteColor,
    ...FONTS.h5,
  },
  flatListBottom: {
    paddingBottom: '100@vs',
    width: SIZES.width - 27,
    marginTop: '25@vs',
  },
  senderList: {
    backgroundColor: COLORS.primaryColor,
    paddingVertical: '9@vs',
    paddingHorizontal: '15@s',
    borderRadius: '20@msr',
    marginVertical: '8@vs',
    alignSelf: 'flex-end',
    maxWidth: '85%',
  },
  receiverList: {
    backgroundColor: COLORS.lightGrey,
    paddingVertical: '9@vs',
    paddingHorizontal: '15@s',
    borderRadius: '20@msr',
    marginVertical: '8@vs',
    alignSelf: 'flex-start',
    maxWidth: '85%',
  },
  senderTxt: {
    color: COLORS.whiteColor,
    ...FONTS.body3,
    lineHeight: '20@vs',
  },
  receiverTxt: {
    color: COLORS.blackColor,
    ...FONTS.body3,
    lineHeight: 20,
  },
  textInput: {
    backgroundColor: COLORS.whiteColor,
    paddingVertical: '6@vs',
    paddingHorizontal: '15@s',
    borderRadius: '25@msr',
    marginVertical: '8@vs',
    borderColor: COLORS.lightGrey,
    borderWidth: '1@s',
    width: SIZES.width - 40,
    flex: 0.7,
    alignSelf: 'flex-start',
  },
  bottomView: {
    width: SIZES.width,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
