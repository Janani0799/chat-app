import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import RootStackScreen from './RootStack';

const Router = () => {
  return (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );
};

export default Router;
