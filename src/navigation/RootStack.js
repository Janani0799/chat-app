import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import RegisterScreen from '../screens/RegisterScreen';
import Messages from '../screens/Messages';
import Chat from '../screens/Chat';

const RootStack = createNativeStackNavigator();

const RootStackScreen = () => (
  <RootStack.Navigator
    screenOptions={{headerShown: false}}
    initialRouteName="RegisterScreen">
    <RootStack.Screen name="RegisterScreen" component={RegisterScreen} />
    <RootStack.Screen name="Messages" component={Messages} />
    <RootStack.Screen name="Chat" component={Chat} />
  </RootStack.Navigator>
);

export default RootStackScreen;
